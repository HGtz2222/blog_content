# Python Cookbook

有些常用的问题的 Python 处理方式, 在这个文章中做出统一记录. 

### 命令行参数处理

> 参考资料: https://blog.csdn.net/weixin_35653315/article/details/72886718

### 使用 SQLite

> 参考资料: http://www.runoob.com/sqlite/sqlite-python.html

### 格式化字符串

f string 的用法. 

```python
f'''insert into blogs values('{title}','{desc}','{content}','','',{tag})'''
```

直接替换变量到字符串中. 类似于 Dart 的操作. 

### base64 编码

编码

```python
content_base64 = base64.b64encode(content.encode('utf-8'))
content_base64 = str(content_base64, 'utf-8')
```

解码

```
base64.b64decode(content)
```

> 参考资料: https://www.cnblogs.com/zanjiahaoge666/p/7242642.html

### 时间戳

```python
time.strftime('%Y%m%d',time.localtime(time.time()))
```

> 参考资料: https://www.cnblogs.com/easont/p/8442503.html

### 使用 DataBase Navigator 插件

数据库操作完毕, 可以借助 PyCharm 的插件快速查看数据库中的内容. 

> 参考资料: https://blog.csdn.net/qq_35056292/article/details/74504871

### Markdown 转为 HTML

基于 mistune 进行转换

基于 pygments 进行代码语法高亮

> 参考资料: https://www.cnblogs.com/WeyneChen/p/6670592.html
>
> TODO 但是这个资料中的细节有点问题. 需要调整. 

### 使用 Flask

搭建一个 HTTP 服务器. 

安装

```sh
pip install flask
```

hello world

```python
from flask import Flask

app = Flask("hello")

@app.route('/')
def hello():
    return "hello"

app.run(debug=True)
```

> 参考资料: https://dormousehole.readthedocs.io/en/latest/