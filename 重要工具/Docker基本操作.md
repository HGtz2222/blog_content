# Docker 基本操作

了解 Docker 是干啥的以及了解 Docker 的基本操作.

## Docker 的基本概念

### Docker 解决的问题

Docker 是一种 **更高性能** 的 **虚拟化技术** (相当于虚拟机如VMWare 的进阶版本).

解决的问题是让开发, 测试, 部署的环境都能完全统一. 

>  **举个栗子**
>
> 在之前的博客系统搭建过程中, 最后一步我们需要把博客系统部署到云服务器上. 
>
> 这个部署的过程很麻烦, 需要安装大量的依赖软件, 并且一旦版本和开发环境不匹配, 就会出现各种问题. 
>
> 而是用 Docker 能够将开发环境依赖的所有外部软件都打包到 **容器** 中, 发布的时候将整个 **容器** 拷贝到云服务器上即可. 

Docker 的特点:

1. 资源隔离(容器和容器之间不会相互影响)
2. 高性能(相比于传统的虚拟机)

在当前的互联网环境中, 尤其是在 **微服务** 的理念下更加举足轻重. 

### 什么是容器

容器是独立运行的一个或一组应用. 

### 什么是镜像

Docker 镜像是用于创建 Docker 容器的模板. 

镜像和容器之间的关系, 类似于可执行文件和进程的关系. 

### 什么是 DockerHub

用于托管镜像的平台. 

## Docker 安装

### Windows

只针对 Windows 10 企业版 可以使用. Windows10 家庭版以及 Win7/Win8 使用起来有很多坑, 暂时不研究. 

### Linux(Centos7)

安装 Docker 及其依赖

```
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
yum makecache fast
yum -y install docker-ce
```

启动 Docker 服务

```
systemctl start docker
```

测试运行 hello-world

```
docker run hello-world
```

## Docker 常用命令

**查看所有命令**

直接敲下 docker 命令, 能看到支持的所有选项. 

```
docker
```

**拉取镜像**

```
docker pull hello-world
```

**运行容器**

如果本地不存在, 则自动拉取

```
docker run hello-world
```

**查看镜像**

```
docker images
```

**查看容器**

```sh
# 只列出运行中的容器
docker ps
# 列出所有容器(包含未运行的)
docker ps -a
```

**停止容器**

```
docker stop [容器ID]
```

**删除镜像**

```
docker rmi [镜像名]
```

注意, 必须把对应的容器先删除掉, 才能删除掉对应的镜像.

**删除容器**

```
docker rm [容器ID]
```



## 使用 Docker 搭建我的世界服务器(TODO)

一条指令即可

```
docker run -ti -d -e TZ=Asia/Shanghai -p 25565:25565 --name mc -d bluerain/minecraft:latest
```

客户端下载

http://www.minecraftxz.com/minecraft1-8-9/



```
docker run -d -it -e EULA=TRUE -e VERSION=1.12.2 -p 25565:25565 --name mc itzg/minecraft-server
```

## 使用 Docker 搭建网盘服务器

手动安装, 参考 https://www.jianshu.com/p/6e0be77b688e 步骤非常麻烦. 而是用 Docker, 只要一个指令就搞定了. 

使用 Docker 安装

```
docker run -d -p 8003:80 owncloud:8.1
```

-p : 将容器中的 80 端口映射到物理机的 8003 端口

-d : 后台运行

然后用浏览器访问

```
http:[你的服务器ip]:8003/index.php
```

此时会弹出一个配置管理员账户的界面. 直接输入即可. 网盘就搭建完成了. 



## 使用 Docker 打包自己的网站

### 在 DockerHub 上找模板

### 写 Dockerfile

### 使用 docker build 构建镜像

### 使用 docker run 运行镜像